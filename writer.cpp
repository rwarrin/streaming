#include <stdio.h>
#include <unistd.h>

int main(void)
{
	FILE *File = fopen("stream.txt", "a");
	if(!File)
	{
		fprintf(stderr, "Failed to open stream.txt");
		return 1;
	}

	int Line = 0;
	for(;;)
	{
		fprintf(File, "%d: some stuff here\n", Line++);
		fflush(File);
		sleep(1);
	}

	return 0;
}
