#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main(void)
{
	FILE *File = fopen("stream.txt", "r");
	if(!File)
	{
		fprintf(stderr, "Failed to open stream.txt");
		return 1;
	}

	char Buffer[1 << 8] = {0};
	int ReachedEnd = 0;
	for(;;)
	{
		Buffer[0] = 0;
		char *Result = fgets(Buffer, 1 << 8, File);
		if(Result)
		{
			if(ReachedEnd)
			{
				if(strstr(Buffer, "6"))
				{
					printf("Magic number found: ");
				}
				printf("%s", Buffer);
			}
		}
		else
		{
			ReachedEnd = 1;
			sleep(1);
		}
	}

	return 0;
}
